===================
PointFinder-3.0
===================

This project documents PointFinder service


Documentation
=============

## What is it?

The PointFinder service contains one python script *PointFinder-3.0.py* which is the script of the lates
version of the PointFinder service. The method detects chromosomal mutations predictive of drug resistance based on WGS data.
This service includes two methods for gene detection, namely KMA and BLASTN. 

## Content of the repository
1. pointfinder-3.0.py     - the program
2. submodules	 - directory with the required blaster submodule 


## Installation

Setting up PointFinder
```bash
# Go to wanted location for resfinder
cd /path/to/some/dir
# Clone and enter the pointfinder directory
git clone --recursive https://bitbucket.org/genomicepidemiology/pointfinder.git
cd pointfinder
```


Installing dependencies:

Biopython: http://biopython.org/DIST/docs/install/Installation.html

Blastn: ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/


# Usage 

The program can be invoked with the -h option to get help and more information of the service.

```bash
     $ python PointFinder.py -i INFILE [INFILE] -o OUTFOLDER 
       -s SPECIES -m METHOD -m_p METHOD_PATH -p DATABASE 
       [-t IDENTITY] [-l COVERAGE] [-u] 

Options:

    -h HELP
       Prints help message

    -i INFILE [INFILE]
       Input file. fastq file(s) from one sample for KMA or 
       one fasta file for blastn.

    -o OUTFOLDER
       Output folder, output files will be stored here.

    -s SPECIES
       Species of choice, e.coli, tuberculosis, salmonella,
       campylobactor, gonorrhoeae, klebsiella, or malaria

    -m METHOD
       Method of choice, kma or blastn	

    -m_p METHOD_PATH
       Path to the location of blastn or kma
       dependent of the chosen method 

    -p DATABASE
       Path to the location of the pointfinder database

    -t IDENTITY
       Minimum gene identity threshold, default = 0.9

    -l COlVERAGE
       Minimum gene coverage threshold = 0.6

    -u UNKNOWN
       Report all mutations found including those that are
       not listed in the mutations library. 
```

## Web-server

A webserver implementing the methods is available as a feature of the ResFinder service at the [CGE website](http://www.genomicepidemiology.org/) and can be found here:
https://cge.cbs.dtu.dk/services/ResFinder-3.0/

The webserver ONLY runs PointFinder with BLASTn on assembled data.


## The Latest Version

The latest version can be found at
https://bitbucket.org/genomicepidemiology/pointfinder/overview


## Documentation

The documentation available as of the date of this release can be found at
https://bitbucket.org/genomicepidemiology/pointfinder/overview.


Citation
=======

When using the method please cite:

Will be added soon

License
=======

Copyright (c) 2014, Ole Lund, Technical University of Denmark
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.